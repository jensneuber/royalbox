#!/bin/bash

if [[ $EUID -ne 0 ]]; then
	echo "You must be a root user" 2>&1
	exit 1
else

	#Upgrade
	apt-get update -qq
	apt-get -y dist-upgrade

	# Install Essentials
	apt-get -y -qq install python-software-properties git ssh

	# Install apache / php / mysql
	apt-get -y -qq install joe apache2 mysql-server php5 php5-mcrypt php5-mysql libapache2-mod-php5 libapache2-mod-dnssd

	# Install Mac integration
	apt-get -y -qq install samba samba-tools libpam-smbpass
	apt-get -y -qq install netatalk
	apt-get -y -qq install avahi-daemon avahi-utils avahi-discover libnss-mdns
	apt-get -y -qq install nfs-kernel-server

	apt-get -y -qq install nodejs npm

	apt-get -y -qq autoclean
	apt-get -y -qq autoremove

fi