## README.md

Nachdem Ubuntu frisch installiert wurde müssen zunächst folgende Commands auf dem Terminal der virtuellen Maschine ausgeführt werden:

## Terminal der virtuellen Maschine

$USERNAME -> bei der Ubuntu-Installation verwendeter Benutzername

	sudo apt-get update
	sudo apt-get dist-upgrade
### -> Neustart
	sudo reboot
	sudo apt-get install git ssh 
	cd ~
	git clone https://jensneuber@bitbucket.org/jensneuber/royalbox.git
	cd royalbox
	git submodule update --init
	sudo ./setup.sh

* rechner-name eingeben (zB web1)
 
* domainname eingeben (eigentlich immer 'local')
 
* mysql-root passwort eingeben (mehrfach)

### -> Neustart !!! WICHTIG
	sudo reboot

## Jetzt im Terminal des Host-Systems (Terminal.app)
 ssh-copy-id -i ~/.ssh/id_rsa.pub $USERNAME@web1.local
