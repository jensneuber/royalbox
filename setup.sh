#!/bin/bash

if [[ $EUID -ne 0 ]]; then
	echo "You must be a root user" 2>&1
	exit 1
else

	usermod -aG www-data,users $SUDO_USER

	SCRIPT_DIR=$(cd $(dirname "$0"); pwd)
	CONFIG_DIR=$SCRIPT_DIR/config
	STATIC_DIR=$SCRIPT_DIR/static
	MODULES_DIR=$SCRIPT_DIR/modules
	TEMPLATE_DIR=$SCRIPT_DIR/templates
	SED_FILE=$SCRIPT_DIR/replace.sed
	NEW_DOMAIN=local

	# Get User Info
	read -p "Bitte den Servernamen eingeben (webX) " NEW_HOST
	#read -p "Bitte die Domain eingeben (local) " NEW_DOMAIN

	if ! [ -a $SED_FILE ]; then
		touch $SED_FILE
		chmod u+rwx $SED_FILE
	fi

	echo 's/${host}/'$NEW_HOST'/g' > $SED_FILE
	echo 's/${domain}/'$NEW_DOMAIN'/g' >> $SED_FILE
	echo 's|${static}|'$STATIC_DIR'|g' >> $SED_FILE
	echo 's|${modules}|'$MODULES_DIR'|g' >> $SED_FILE

	mkdir /usr/local/lib/node_modules
	# chmod 777 /usr/local/lib/node_modules
	# chmod 777 /usr/local/lib
	# chmod 777 /usr/local/share
	# chmod 777 /usr/local/bin

	#npm install dryice -g

	#export NODE_PATH="/usr/local/lib/node_modules"

	#chmod +x $MODULES_DIR/ace/install.js && $MODULES_DIR/ace/install.js

	setupDate=$(date +"%Y%m%d-%H:%M:%S")

	###ENGAGE TEMPLATES
	sed -f $SED_FILE $TEMPLATE_DIR/hosts.template > $CONFIG_DIR/etc/hosts
	sed -f $SED_FILE $TEMPLATE_DIR/hostname.template > $CONFIG_DIR/etc/hostname
	sed -f $SED_FILE $TEMPLATE_DIR/avahi/avahi-daemon.template > $CONFIG_DIR/etc/avahi/avahi-daemon.conf
	sed -f $SED_FILE $TEMPLATE_DIR/netatalk/AppleVolumes.default.template > $CONFIG_DIR/etc/netatalk/AppleVolumes.default
	sed -f $SED_FILE $TEMPLATE_DIR/apache2/apache2.template > $CONFIG_DIR/etc/apache2/apache2.conf
	sed -f $SED_FILE $TEMPLATE_DIR/apache2/sites-available/newconfig.template > $CONFIG_DIR/etc/apache2/sites-available/$NEW_HOST

	### AVAHI-Aliases
	git clone https://github.com/airtonix/avahi-aliases.git ~/avahi-aliases
	~/install.sh


##### FILES
	FILES[0]="/etc/hosts"
	FILES[1]="/etc/hostname"
	FILES[2]="/etc/default/netatalk"
	FILES[3]="/etc/netatalk/afpd.conf"
	FILES[4]="/etc/netatalk/AppleVolumes.default"
	FILES[5]="/etc/nsswitch.conf"
	FILES[6]="/etc/avahi/avahi-daemon.conf"
# 	FILES[7]="/etc/avahi/services/multi.service"
	FILES[7]="/etc/exports"
	FILES[8]="/etc/apache2/apache2.conf"
	FILES[9]="/etc/apache2/sites-available/$NEW_HOST"
	FILES[10]="/etc/samba/smb.conf"
# 	FILES[11]="/etc/logrotate.d/apache2"

	for FILE in ${FILES[@]}
	do
		if [ -f $FILE ]; then
			echo "## Renaming Original $FILE to $FILE.$setupDate.back"
			mv $FILE $FILE.$setupDate.back
		fi
		echo "## $FILE verlinken"
		ln -sf $CONFIG_DIR$FILE $FILE
	done

	cp $CONFIG_DIR/etc/avahi/services/multi.service /etc/avahi/services/multi.service
	cp $CONFIG_DIR/.bashrc ~/.bashrc
	cp $CONFIG_DIR/etc/logrotate.d/apache2 /etc/logrotate.d/apache2

	# APACHE
	echo "## Linking new Apache site"
	ln -f $CONFIG_DIR/etc/apache2/sites-available/$NEW_HOST /etc/apache2/sites-available/$NEW_HOST
	if [ -e /var/www/index.html ]; then
		mv /var/www/index.html /var/www/index.html.back
	fi
	chown -R www-data:www-data /var/www

	a2enmod autoindex
	a2dissite default
	a2dissite default-ssl
	a2ensite $NEW_HOST

	cp $CONFIG_DIR/phpMyAdmin/config.inc.php $MODULES_DIR/phpMyAdmin/config.inc.php

	# PHPMYADMIN
	echo "## Configuring phpMyAdmin"
	read -p "Bitte das MYSQL root passwort eingeben: " mysqlpass
	mysql -u root --password=$mysqlpass < $CONFIG_DIR/etc/mysql/myconfig.sql
	mysql -u root --password=$mysqlpass < $MODULES_DIR/phpMyAdmin/examples/create_tables.sql

	service netatalk restart

	service apache2 restart
	service smbd restart
	service nfs-kernel-server restart
	service avahi-daemon restart

	echo '#########'
	echo 'Bitte ein neues Passwort für den Benutzer www-data eingeben'
	passwd www-data


fi